/*!
 * @file 		register.cpp
 * @author 		Jiri Melnikov
 * @date		17.2.2019
 * @copyright	Jiri Melnikov
 * 				Distributed BSD License
 *
 */

#include "EuroliteDMX.h"
#include "yuri/core/thread/IOThreadGenerator.h"

namespace yuri {
namespace ftdidmx {

MODULE_REGISTRATION_BEGIN("ftdidmx")
		REGISTER_IOTHREAD("eurolitedmx",EuroliteDMX)
MODULE_REGISTRATION_END()

}
}
