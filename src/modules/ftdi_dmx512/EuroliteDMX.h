/*!
 * @file 		EuroliteDMX.h
 * @author 		Jiri Melnikov
 * @date 		17.2.2019
 * @copyright	Jiri Melnikov
 * 				Distributed BSD License
 *
 */

#ifndef EUROLITEDMX_H_
#define EUROLITEDMX_H_

#include "FtdiDMX512.h"
#include "yuri/core/thread/IOThread.h"
#include "yuri/event/BasicEventConsumer.h"
#include "yuri/core/utils/color.h"

namespace yuri {
namespace ftdidmx {

class EuroliteDMX: public FtdiDMX512, public event::BasicEventConsumer
{
public:
	IOTHREAD_GENERATOR_DECLARATION
	EuroliteDMX(const log::Log &log_, core::pwThreadBase parent, const core::Parameters &parameters);
	static core::Parameters configure();
	virtual ~EuroliteDMX() noexcept;

protected:
	virtual void run() override;
	virtual bool set_param(const core::Parameter& param) override;
	virtual bool do_process_event(const std::string& event_name, const event::pBasicEvent& event) override;
	void update_packet();

	core::color_t color_;
	int intensity_;
	int channel_;
};

} /* namespace ftdidmx */
} /* namespace yuri */

#endif /* EUROLITEDMX_H_ */
