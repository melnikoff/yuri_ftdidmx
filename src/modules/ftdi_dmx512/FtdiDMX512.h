/*!
 * @file 		FtdiDMX512.h
 * @author 		Jiri Melnikov
 * @date 		17.2.2019
 * @copyright	Jiri Melnikov
 * 				Distributed BSD License
 *
 */

#ifndef FTDIDMX512_H_
#define FTDIDMX512_H_

#include <libftdi1/ftdi.h>

#include "yuri/core/thread/IOThread.h"

namespace yuri {
namespace ftdidmx {

class FtdiDMX512: public core::IOThread
{
public:
	static core::Parameters configure();
	virtual ~FtdiDMX512() noexcept;
	virtual bool set_param(const core::Parameter& param) override;
	virtual void run() override;

protected:
	FtdiDMX512(const log::Log &log_, core::pwThreadBase parent, const std::string& name);

	static const int packet_len_ = 513;
	uint8_t packet_[packet_len_];

	bool changed_;
	std::string device_;
	bool ff_;

	struct ftdi_context *ftdi_;
    struct ftdi_version_info version_;
};

} /* namespace ftdidmx */
} /* namespace yuri */

#endif /* FTDIDMX512_H_ */
