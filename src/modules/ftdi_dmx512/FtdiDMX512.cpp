/*!
 * @file 		FtdiDMX512.cpp
 * @author 		Jiri Melnikov
 * @date		17.2.2019
 * @copyright	Jiri Melnikov
 * 				Distributed BSD License
 *
 */

#include "FtdiDMX512.h"

#include "yuri/core/Module.h"
#include "yuri/event/EventHelpers.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>

namespace yuri {
namespace ftdidmx {

core::Parameters FtdiDMX512::configure()
{
	core::Parameters p = core::IOThread::configure();
	p["device"]["USB device name"]="";
	p["ff"]["Use first found device"]=true;
	return p;
}


FtdiDMX512::FtdiDMX512(const log::Log &log_, core::pwThreadBase parent, const std::string& name)
:core::IOThread(log_, parent, 0, 0, name),device_(""),ff_(true)
{
	// Init FTDI driver
	if ((ftdi_ = ftdi_new()) == 0)
		exception::InitializationFailed("Failed to initialize FTDI.");
    if (ftdi_usb_open(ftdi_, 0x0403, 0x6001) < 0)
		exception::InitializationFailed("Failed to open USB FTDI device: "+std::string(ftdi_get_error_string(ftdi_)));
	if (ftdi_usb_reset(ftdi_) < 0)
		exception::InitializationFailed("Failed to reset device: "+std::string(ftdi_get_error_string(ftdi_)));
	if (ftdi_set_baudrate(ftdi_, 250000) < 0)
		exception::InitializationFailed("Failed to set baudrate: "+std::string(ftdi_get_error_string(ftdi_)));
	if (ftdi_set_line_property(ftdi_, BITS_8, STOP_BIT_2, NONE) < 0)
		exception::InitializationFailed("Failed to set line parameters: "+std::string(ftdi_get_error_string(ftdi_)));
	if (ftdi_setflowctrl(ftdi_, SIO_DISABLE_FLOW_CTRL) < 0)
		exception::InitializationFailed("Failed to set flow control: "+std::string(ftdi_get_error_string(ftdi_)));
	if (ftdi_setrts(ftdi_, 0) < 0)
		exception::InitializationFailed("Failed to set rts: "+std::string(ftdi_get_error_string(ftdi_)));
	if (ftdi_usb_purge_buffers(ftdi_) < 0)
		exception::InitializationFailed("Failed to purge buffers: "+std::string(ftdi_get_error_string(ftdi_)));

	// Clear packet buffer
	memset(packet_, 0, packet_len_);
}

FtdiDMX512::~FtdiDMX512() noexcept
{
}

void FtdiDMX512::run()
{
	if (changed_) {
		changed_ = false;
		ftdi_set_line_property2(ftdi_, BITS_8, STOP_BIT_2, NONE, BREAK_ON);
		ftdi_set_line_property2(ftdi_, BITS_8, STOP_BIT_2, NONE, BREAK_OFF);
		int bytes_sent;
		if ((bytes_sent = ftdi_write_data(ftdi_, packet_, packet_len_)) != packet_len_) {
			log[log::warning] << "Invalid bytes count sent: " << bytes_sent << ", seems like DMX problem.";
		}
		usleep(1000*5);
	}	
}

bool FtdiDMX512::set_param(const core::Parameter& param)
{
	if (assign_parameters(param)
			(device_, "device")
			(ff_, "ff"))
		return true;
	return core::IOThread::set_param(param);
}

} /* namespace ftdidmx */
} /* namespace yuri */
