/*!
 * @file 		EuroliteDMX.cpp
 * @author 		Jiri Melnikov
 * @date		17.2.2019
 * @copyright	Jiri Melnikov
 * 				Distributed BSD License
 *
 */

#include "EuroliteDMX.h"

#include "yuri/core/Module.h"
#include "yuri/event/EventHelpers.h"
#include "yuri/core/utils/assign_events.h"

#include <stdio.h>
#include <string.h>

namespace yuri {
namespace ftdidmx {

IOTHREAD_GENERATOR(EuroliteDMX)

core::Parameters EuroliteDMX::configure() {
	core::Parameters p = FtdiDMX512::configure();
	p.set_description("Sends ArtNet packets for light control.");
	p["color"]["Color to set"]=core::color_t::create_rgb(255, 255, 255);
	p["intensity"]["Light intensity (0-255)"]=128;
	p["channel"]["DMX channel"]=0;
	return p;
}


EuroliteDMX::EuroliteDMX(const log::Log &log_, core::pwThreadBase parent, const core::Parameters &parameters)
:FtdiDMX512(log_,parent,"eurolitedmx"),event::BasicEventConsumer(log),
color_(core::color_t::create_rgb(255, 255, 255)),intensity_(0),channel_(0) {
	IOTHREAD_INIT(parameters)
}

EuroliteDMX::~EuroliteDMX() noexcept {}

void EuroliteDMX::run() {
	update_packet();
	while(still_running()){
		wait_for_events(get_latency());
		process_events();
		FtdiDMX512::run();
	}	
}


bool EuroliteDMX::do_process_event(const std::string& event_name, const event::pBasicEvent& event) {
	if (assign_events(event_name, event)
		(color_, "color")
		.parsed<uint8_t>(color_, "r", [this](uint8_t val){return core::color_t::create_rgb(val,color_.get_rgb()[1],color_.get_rgb()[2]);})
		.parsed<uint8_t>(color_, "g", [this](uint8_t val){return core::color_t::create_rgb(color_.get_rgb()[0],val,color_.get_rgb()[2]);})
		.parsed<uint8_t>(color_, "b", [this](uint8_t val){return core::color_t::create_rgb(color_.get_rgb()[0],color_.get_rgb()[1],val);})
		(channel_, "channel")
		(intensity_, "intensity")) {
			update_packet();
        	return true;
		}
	return false;
}

bool EuroliteDMX::set_param(const core::Parameter& param) {
	if (assign_parameters(param)
			(color_,   "color")
			(channel_, "channel")
			(intensity_, "intensity"))
		return true;
	return FtdiDMX512::set_param(param);
}

void EuroliteDMX::update_packet() {
	memset(packet_, 0, packet_len_);
	packet_[1+channel_+0] = color_.get_rgb()[0];
	packet_[1+channel_+1] = color_.get_rgb()[1];
	packet_[1+channel_+2] = color_.get_rgb()[2];
	packet_[1+channel_+6] = intensity_;
	changed_ = true;
}

} /* namespace ftdidmx */
} /* namespace yuri */
